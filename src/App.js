import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "./App.css";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Home from "./pages/Home";
import Communities from "./pages/Communities";
import CommunityPage from "./pages/CommunityPage";
import PostDetail from "./pages/PostDetail";
import ProfilePage from "./pages/ProfilePage";
import AboutPage from "./pages/AboutPage";
import ExplorePage from "./pages/ExplorePage";
import { loadData } from "./utils/DataLoader";

function App() {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      await loadData();
      setLoading(false);
    };
    fetchData();
  }, []);

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <div className="App">
      <Router>
        <Header />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/communities" element={<Communities />} />
          <Route path="/explore" element={<ExplorePage />} />
          <Route path="/about" element={<AboutPage />} />
          <Route path="/:communityName" element={<CommunityPage />} />
          <Route path="/:communityName/:postId" element={<PostDetail />} />
          <Route path="/profile" element={<ProfilePage />} />
        </Routes>
      </Router>
      <Footer />
    </div>
  );
}

export default App;
