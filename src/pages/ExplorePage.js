import React, { useState, useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import Feed from "../components/Feed";
import "../styles/ExplorePage.css";

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

function ExplorePage() {
  const [posts, setPosts] = useState([]);
  const [tags, setTags] = useState([]);
  const [locations, setLocations] = useState([]);
  const [selectedTags, setSelectedTags] = useState([]);
  const [selectedLocations, setSelectedLocations] = useState([]);
  const [searchMode, setSearchMode] = useState("any");
  const query = useQuery();
  const location = useLocation();
  const navigate = useNavigate();

  useEffect(() => {
    const storedData = localStorage.getItem("data");
    const data = JSON.parse(storedData);
    setPosts(data);
    extractUniqueTags(data);
    extractUniqueLocations(data);
  }, []);

  useEffect(() => {
    const tagsFromUrl = query.get("tags");
    const locationsFromUrl = query.get("locations");
    if (tagsFromUrl) {
      setSelectedTags(tagsFromUrl.split(","));
    } else {
      setSelectedTags([]);
    }
    if (locationsFromUrl) {
      setSelectedLocations(locationsFromUrl.split(","));
    } else {
      setSelectedLocations([]);
    }
  }, [query, location.search]); // Trigger this effect whenever query or the search part of the URL changes

  const extractUniqueTags = (data) => {
    const allTags = data.flatMap((post) => post.tags);
    const uniqueTags = [...new Set(allTags)];
    setTags(uniqueTags);
  };

  const extractUniqueLocations = (data) => {
    const allLocations = data.map((post) => post.location).filter(Boolean); // Filter out empty locations
    const uniqueLocations = [...new Set(allLocations)];
    setLocations(uniqueLocations);
  };

  const handleTagChange = (e) => {
    const tag = e.target.value;
    let updatedTags = [];

    if (selectedTags.includes(tag)) {
      updatedTags = selectedTags.filter((t) => t !== tag);
    } else {
      updatedTags = [...selectedTags, tag];
    }

    setSelectedTags(updatedTags);

    updateURLParams(updatedTags, selectedLocations);
  };

  const handleLocationChange = (e) => {
    const location = e.target.value;
    let updatedLocations = [];

    if (selectedLocations.includes(location)) {
      updatedLocations = selectedLocations.filter((l) => l !== location);
    } else {
      updatedLocations = [...selectedLocations, location];
    }

    setSelectedLocations(updatedLocations);

    updateURLParams(selectedTags, updatedLocations);
  };

  const updateURLParams = (updatedTags, updatedLocations) => {
    const params = new URLSearchParams();
    if (updatedTags.length > 0) {
      params.set("tags", updatedTags.join(","));
    } else {
      params.delete("tags");
    }
    if (updatedLocations.length > 0) {
      params.set("locations", updatedLocations.join(","));
    } else {
      params.delete("locations");
    }
    navigate({
      pathname: location.pathname,
      search: params.toString(),
    });
  };

  const handleSearchModeChange = (e) => {
    setSearchMode(e.target.value);
  };

  const filteredPosts =
    selectedTags.length === 0 && selectedLocations.length === 0
      ? posts
      : searchMode === "all"
      ? posts.filter(
          (post) =>
            selectedTags.every((tag) => post.tags.includes(tag)) &&
            selectedLocations.every((loc) => post.location === loc)
        )
      : posts.filter(
          (post) =>
            post.tags.some((tag) => selectedTags.includes(tag)) ||
            selectedLocations.includes(post.location)
        );

  return (
    <div className="explore-page">
      <h1 className="explore-page-title">
        Explore posts from communities
      </h1>
      <div className="search-mode">
        <label>
          <input
            type="radio"
            value="any"
            checked={searchMode === "any"}
            onChange={handleSearchModeChange}
          />
          Any tag or location
        </label>
        <label>
          <input
            type="radio"
            value="all"
            checked={searchMode === "all"}
            onChange={handleSearchModeChange}
          />
          All tags and locations
        </label>
      </div>
      <div className="content">
        <div className="filters">
          <div className="tags-filter">
            <h3>Filter by Tags</h3>
            {tags.map((tag) => (
              <div key={tag} className="tag-item">
                <input
                  type="checkbox"
                  id={tag}
                  value={tag}
                  checked={selectedTags.includes(tag)}
                  onChange={handleTagChange}
                />
                <label htmlFor={tag}>{tag}</label>
              </div>
            ))}
          </div>
          <div className="locations-filter">
            <h3>Filter by Locations</h3>
            {locations.map((loc) => (
              <div key={loc} className="location-item">
                <input
                  type="checkbox"
                  id={loc}
                  value={loc}
                  checked={selectedLocations.includes(loc)}
                  onChange={handleLocationChange}
                />
                <label htmlFor={loc}>{loc}</label>
              </div>
            ))}
          </div>
        </div>
        <Feed feed={filteredPosts} />
      </div>
    </div>
  );
}

export default ExplorePage;
