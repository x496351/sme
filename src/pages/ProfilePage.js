import React, { useState, useEffect } from "react";
import CommunitiesFeed from "../components/CommunitiesFeed";
import Feed from "../components/Feed";
import "../styles/ProfilePage.css";

function ProfilePage() {
  const [activeTab, setActiveTab] = useState("Followed Communities");
  const [user, setUser] = useState(null);
  const [followedCommunities, setFollowedCommunities] = useState([]);
  const [myCommunities, setMyCommunities] = useState([]);
  const [userPosts, setUserPosts] = useState([]);

  useEffect(() => {
    const storedUsers = localStorage.getItem("users");
    const users = JSON.parse(storedUsers);
    const user = users.find((u) => u.user_id === 1);
    setUser(user);

    const storedCommunities = localStorage.getItem("communities");
    const communities = JSON.parse(storedCommunities);
    const createdOrManaged = communities.filter((community) =>
      user.communities.includes(community.id)
    );
    const followed = communities.filter((community) =>
      community.user_ids.includes(user.user_id)
    );

    setFollowedCommunities(followed);
    setMyCommunities(createdOrManaged);

    const storedData = localStorage.getItem("data");
    const data = JSON.parse(storedData);
    const userCreatedPosts = data.filter((post) => post.posted_by === user.username);
    const userCommentedPosts = data.filter((post) =>
      post.comments.some((comment) => comment.user === user.username)
    );
    const likedPosts = data.filter((post) => localStorage.getItem(`liked-${post.id}`) === "true");
    const dislikedPosts = data.filter((post) => localStorage.getItem(`disliked-${post.id}`) === "true");
    const ratedPosts = data.filter((post) => localStorage.getItem(`rated-${post.id}`) !== null);

    setUserPosts([...new Set([...userCreatedPosts, ...userCommentedPosts, ...likedPosts, ...dislikedPosts, ...ratedPosts])]);
  }, []);

  const renderContent = () => {
    switch (activeTab) {
      case "Followed Communities":
        return (
          <div>
            <h2>Communities I follow</h2>
            <CommunitiesFeed communities={followedCommunities} />
          </div>
        );
      case "My Communities":
        return (
          <div>
            <h2>Communities founded by me</h2>
            <CommunitiesFeed communities={myCommunities} />
          </div>
        );
      case "Activity":
        return (
          <div>
            <h2>Activity</h2>
            <Feed feed={userPosts} />
          </div>
        );
      case "Bio":
        return (
          <div className="profile">
            <div className="bio">
              <h2>Bio</h2>
              <p>Username: {user.username}</p>
              <p>Points: {user.points}</p>
              <p>Level: {user.level}</p>
            </div>
            <div className="details">
              <div className="column">
                <h3>Badges</h3>
                <ul>
                  {user.badges.map((badge, index) => (
                    <li key={index}>{badge}</li>
                  ))}
                </ul>
                <h3>Event Participation</h3>
                <ul>
                  {user.event_participation.map((event, index) => (
                    <li key={index}>{event}</li>
                  ))}
                </ul>
              </div>
              <div className="column">
                <h3>Challenges</h3>
                <ul>
                  {Object.entries(user.challenges).map(
                    ([challenge, status], index) => (
                      <li key={index}>
                        {challenge}: {status}
                      </li>
                    )
                  )}
                </ul>
                <h3>Rewards Redeemed</h3>
                <ul>
                  {user.reward_redeemed.map((reward, index) => (
                    <li key={index}>{reward}</li>
                  ))}
                </ul>
              </div>
            </div>
          </div>
        );
        
        /*return (
          <div>
            <h2>Bio</h2>
            <p>Username: {user.username}</p>
            <p>Points: {user.points}</p>
            <p>Level: {user.level}</p>
            <h3>Badges</h3>
            <ul>
              {user.badges.map((badge, index) => (
                <li key={index}>{badge}</li>
              ))}
            </ul>
            <h3>Challenges</h3>
            <ul>
              {Object.entries(user.challenges).map(
                ([challenge, status], index) => (
                  <li key={index}>
                    {challenge}: {status}
                  </li>
                )
              )}
            </ul>
            <h3>Event Participation</h3>
            <ul>
              {user.event_participation.map((event, index) => (
                <li key={index}>{event}</li>
              ))}
            </ul>
            <h3>Rewards Redeemed</h3>
            <ul>
              {user.reward_redeemed.map((reward, index) => (
                <li key={index}>{reward}</li>
              ))}
            </ul>
          </div>
        );*/
    }
  };

  if (!user) {
    return <p>Loading...</p>;
  }

  return (
    <div className="profile-page">
      <h1 className="profile-page-title">Profile</h1>
      <div className="tabs">
        <button
          className={`tab ${
            activeTab === "Followed Communities" ? "active" : ""
          }`}
          onClick={() => setActiveTab("Followed Communities")}
        >
          Followed Communities
        </button>
        <button
          className={`tab ${activeTab === "My Communities" ? "active" : ""}`}
          onClick={() => setActiveTab("My Communities")}
        >
          My Communities
        </button>
        <button
          className={`tab ${activeTab === "Activity" ? "active" : ""}`}
          onClick={() => setActiveTab("Activity")}
        >
          Activity
        </button>
        <button
          className={`tab ${activeTab === "Bio" ? "active" : ""}`}
          onClick={() => setActiveTab("Bio")}
        >
          Bio
        </button>
      </div>
      <div>{renderContent()}</div>
    </div>
  );
}

export default ProfilePage;
