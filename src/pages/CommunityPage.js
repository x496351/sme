import React, { useState, useEffect } from "react";
import Feed from "../components/Feed";
import Modal from "../components/Modal";
import { useParams } from "react-router-dom";
import "../styles/CommunityPage.css";

function CommunityPage() {
  const { communityName } = useParams();
  const [feed, setFeed] = useState([]);
  const [members, setMembers] = useState([]);
  const [activeTab, setActiveTab] = useState("Posts");
  const [showModal, setShowModal] = useState(false);
  const [isMember, setIsMember] = useState(false);
  const [formData, setFormData] = useState({
    content: "",
    details: "",
    location: "",
    type: "post",
    tags: "",
  });

  useEffect(() => {
    const storedData = localStorage.getItem("data");
    const data = JSON.parse(storedData);
    const filteredData = data.filter(
      (item) => item.community === communityName
    );
    setFeed(filteredData);

    const storedCommunities = localStorage.getItem("communities");
    const communities = JSON.parse(storedCommunities);
    const community = communities.find((c) => c.name === communityName);
    const storedUsers = localStorage.getItem("users");
    const users = JSON.parse(storedUsers);
    const communityMembers = users.filter((user) =>
      community.user_ids.includes(user.user_id)
    );
    setMembers(communityMembers);

    // Check if user with id 1 is a member
    setIsMember(community.user_ids.includes(1));
  }, [communityName]);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();
    const newPost = {
      ...formData,
      id: Date.now(),
      community: communityName,
      tags: formData.tags.split(",").map((tag) => tag.trim()),
      likes: 0,
      comments: [],
      posted_by: "john_doe", // User with id 1
    };

    const storedData = localStorage.getItem("data");
    const data = storedData ? JSON.parse(storedData) : [];
    data.push(newPost);
    localStorage.setItem("data", JSON.stringify(data));

    setFeed([...feed, newPost]);
    setShowModal(false);
    setFormData({
      content: "",
      details: "",
      location: "",
      type: "post",
      tags: "",
    });
  };

  const handleJoinLeave = () => {
    const storedCommunities = localStorage.getItem("communities");
    const communities = JSON.parse(storedCommunities);
    const communityIndex = communities.findIndex(
      (c) => c.name === communityName
    );

    if (communityIndex !== -1) {
      const userIndex = communities[communityIndex].user_ids.indexOf(1);
      if (userIndex === -1) {
        // Add user with id 1
        communities[communityIndex].user_ids.push(1);
        setIsMember(true);
      } else {
        // Remove user with id 1
        communities[communityIndex].user_ids.splice(userIndex, 1);
        setIsMember(false);
      }
      localStorage.setItem("communities", JSON.stringify(communities));
      updateCommunityMembers(communities[communityIndex].user_ids);
    }
  };

  const updateCommunityMembers = (user_ids) => {
    const storedUsers = localStorage.getItem("users");
    const users = JSON.parse(storedUsers);
    const communityMembers = users.filter((user) =>
      user_ids.includes(user.user_id)
    );
    setMembers(communityMembers);
  };

  const renderContent = () => {
    switch (activeTab) {
      case "Posts":
        return feed.filter((item) => item.type === "post").length > 0 ? <Feed feed={feed.filter((item) => item.type === "post")} /> : <p className="nothing-here">There is nothing here yet</p>;
      case "Events":
        return feed.filter((item) => item.type === "event").length > 0 ? <Feed feed={feed.filter((item) => item.type === "event")} /> : <p className="nothing-here">There is nothing here yet</p>;
      case "About":
        return (
          <div>
            <h2>About {communityName}</h2>
            <p>
              This community is dedicated to {communityName.toLowerCase()}{" "}
              enthusiasts.
            </p>
          </div>
        );
      case "Rules":
        return (
          <div className="rules-container">
            <h2>Rules for {communityName}</h2>
            <ul className="rules-list">
              <li>Be respectful</li>
              <li>No spamming</li>
              <li>Keep discussions on topic</li>
            </ul>
          </div>
        );
      case "Members":
        return (
          <div>
            <h2>Members of {communityName}</h2>
            <ul className="members-list">
              {members.map((member) => (
                <li key={member.user_id} className="member-item">
                  <h3>{member.username}</h3>
                  <p>Level: {member.level}</p>
                  <p>Points: {member.points}</p>
                  <p>Badges: {member.badges.join(", ")}</p>
                  <p>Referrals: {member.referrals}</p>
                  <p>Challenges:</p>
                  <ul>
                    {Object.entries(member.challenges).map(
                      ([challenge, status], index) => (
                        <li key={index}>
                          {challenge}: {status}
                        </li>
                      )
                    )}
                  </ul>
                  <p>Event Participation:</p>
                  <ul>
                    {member.event_participation.map((event, index) => (
                      <li key={index}>{event}</li>
                    ))}
                  </ul>
                  <p>Rewards Redeemed:</p>
                  <ul>
                    {member.reward_redeemed.map((reward, index) => (
                      <li key={index}>{reward}</li>
                    ))}
                  </ul>
                </li>
              ))}
            </ul>
          </div>
        );
      default:
        return null;
    }
  };

  return (
    <div className="community-page">
      <h1 className="community-title">Welcome to {communityName}</h1>
      <div className="tabs">
        <button
          className={`tab ${activeTab === "Posts" ? "active" : ""}`}
          onClick={() => setActiveTab("Posts")}
        >
          Posts
        </button>
        <button
          className={`tab ${activeTab === "Events" ? "active" : ""}`}
          onClick={() => setActiveTab("Events")}
        >
          Events
        </button>
        <button
          className={`tab ${activeTab === "About" ? "active" : ""}`}
          onClick={() => setActiveTab("About")}
        >
          About
        </button>
        <button
          className={`tab ${activeTab === "Rules" ? "active" : ""}`}
          onClick={() => setActiveTab("Rules")}
        >
          Rules
        </button>
        <button
          className={`tab ${activeTab === "Members" ? "active" : ""}`}
          onClick={() => setActiveTab("Members")}
        >
          Members
        </button>
      </div>
      <button className="new-post-button" onClick={() => setShowModal(true)}>
        Create New Post/Event
      </button>
      <button className="join-leave-button" onClick={handleJoinLeave}>
        {isMember ? "Unfollow" : "Follow"}
      </button>
      <Modal show={showModal} handleClose={() => setShowModal(false)}>
        <form className="create-form" onSubmit={handleFormSubmit}>
          <input
            type="text"
            name="content"
            value={formData.content}
            onChange={handleInputChange}
            placeholder="Content"
            required
          />
          <textarea
            name="details"
            value={formData.details}
            onChange={handleInputChange}
            placeholder="Details"
            required
          />
          <input
            type="text"
            name="location"
            value={formData.location}
            onChange={handleInputChange}
            placeholder="Location"
          />
          <select
            name="type"
            value={formData.type}
            onChange={handleInputChange}
          >
            <option value="post">Post</option>
            <option value="event">Event</option>
          </select>
          <input
            type="text"
            name="tags"
            value={formData.tags}
            onChange={handleInputChange}
            placeholder="Tags (comma separated)"
          />
          <button type="submit">Submit</button>
        </form>
      </Modal>
      <div>{renderContent()}</div>
    </div>
  );
}

export default CommunityPage;
