import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import Comments from "../components/Comments";
import "../styles/PostDetail.css";

const PostDetail = () => {
  const { postId } = useParams();
  const [post, setPost] = useState(null);
  const [likes, setLikes] = useState(0);
  const [rating, setRating] = useState(0);
  const [liked, setLiked] = useState(false);
  const [disliked, setDisliked] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    const storedData = localStorage.getItem("data");
    if (storedData) {
      const data = JSON.parse(storedData);
      const fetchedPost = data.find((p) => p.id === parseInt(postId));
      setPost(fetchedPost);
      setLikes(fetchedPost.likes || 0);
      setRating(fetchedPost.rating || 0);
    }
    const storedLiked = localStorage.getItem(`liked-${postId}`);
    const storedDisliked = localStorage.getItem(`disliked-${postId}`);
    if (storedLiked) {
      setLiked(true);
    }
    if (storedDisliked) {
      setDisliked(true);
    }
  }, [postId]);

  if (!post) {
    return <p>Loading...</p>;
  }

  const handleLike = () => {
    if (disliked) {
      setLikes(likes + 2);
      setDisliked(false);
      localStorage.removeItem(`disliked-${post.id}`);
    } else {
      setLikes(likes + 1);
    }
    setLiked(true);
    localStorage.setItem(`liked-${post.id}`, true);
    updateLikes(post.id, likes + 1);
  };

  const handleDislike = () => {
    if (liked) {
      setLikes(likes - 2);
      setLiked(false);
      localStorage.removeItem(`liked-${post.id}`);
    } else {
      setLikes(likes - 1);
    }
    setDisliked(true);
    localStorage.setItem(`disliked-${post.id}`, true);
    updateLikes(post.id, likes - 1);
  };

  const handleRatingChange = (newRating) => {
    setRating(newRating);
    localStorage.setItem(`rated-${post.id}`, newRating);
    updateRating(post.id, newRating);
  };

  const updateLikes = (id, newLikes) => {
    const storedData = JSON.parse(localStorage.getItem("data"));
    const updatedData = storedData.map((post) =>
      post.id === id ? { ...post, likes: newLikes } : post
    );
    localStorage.setItem("data", JSON.stringify(updatedData));
  };

  const updateRating = (id, newRating) => {
    const storedData = JSON.parse(localStorage.getItem("data"));
    const updatedData = storedData.map((post) =>
      post.id === id ? { ...post, rating: newRating } : post
    );
    localStorage.setItem("data", JSON.stringify(updatedData));
  };

  const handleTagClick = (tag) => {
    navigate(`/explore?tags=${encodeURIComponent(tag)}`);
  };

  const handleAddComment = (newComment) => {
    const updatedComments = [...post.comments, newComment];
    setPost({ ...post, comments: updatedComments });
    updateComments(post.id, updatedComments);
  };

  const updateComments = (id, newComments) => {
    const storedData = JSON.parse(localStorage.getItem("data"));
    const updatedData = storedData.map((p) =>
      p.id === id ? { ...p, comments: newComments } : p
    );
    localStorage.setItem("data", JSON.stringify(updatedData));
  };

  return (
    <div className="post-detail">
      <div className="post-detail-header">
        <h1>{post.content}</h1>
        <a href={`/${post.community}`} className="feed-item-community">
          {post.community}
        </a>
      </div>
      <div className="post-content-detail">
        <p>{post.details}</p>
      </div>
      <div className="post-actions">
        {post.type === "post" ? (
          <div className="button-group">
            <button onClick={handleLike} disabled={liked}>
              Like
            </button>
            <button onClick={handleDislike} disabled={disliked}>
              Dislike
            </button>
            <button>Share</button>
            <span className="likes-count">{likes} Likes</span>
          </div>
        ) : (
          <div className="star-rating">
            {[1, 2, 3, 4, 5].map((star) => (
              <span
                key={star}
                className={`star ${star <= rating ? "filled" : ""}`}
                onClick={() => handleRatingChange(star)}
              >
                ★
              </span>
            ))}
            <span className="current-rating">{rating.toFixed(1)} Rating</span>
          </div>
        )}
        <div className="post-detail-tags">
          {post.tags &&
            post.tags.map((tag, index) => (
              <span
                key={index}
                className="post-detail-tag-link"
                onClick={() => handleTagClick(tag)}
              >
                {tag}
              </span>
            ))}
        </div>
      </div>
      <Comments comments={post.comments || []} onAddComment={handleAddComment} />
    </div>
  );
};

export default PostDetail;
