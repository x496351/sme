import React, { useState, useEffect } from "react";
import Feed from "../components/Feed";
import "../styles/Home.css";

function Home() {
  const [feed, setFeed] = useState([]);

  useEffect(() => {
    const storedData = localStorage.getItem("data");
    setFeed(JSON.parse(storedData))
  }, []);

  return (
    <div className="home">
      <h1>News from communities</h1>
      <Feed feed={feed} />
    </div>
  );
}

export default Home;
