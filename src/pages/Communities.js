import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Communities from "../components/CommunitiesFeed";
import Modal from "../components/Modal";
import "../styles/Communities.css";

function CommunitiesPage() {
  const [uniqueCommunities, setCommunities] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [formData, setFormData] = useState({
    name: "",
  });
  const navigate = useNavigate();

  useEffect(() => {
    const storedData = localStorage.getItem("communities");
    if (storedData) {
      const data = JSON.parse(storedData);
      setCommunities(data);
    }
  }, []);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();
    const newCommunity = {
      ...formData,
      id: Date.now(),
      user_ids: [1], // Add user with id 1
    };

    // Update communities in localStorage
    const storedCommunities = localStorage.getItem("communities");
    const communities = storedCommunities ? JSON.parse(storedCommunities) : [];
    communities.push(newCommunity);
    localStorage.setItem("communities", JSON.stringify(communities));

    // Update the user's communities array in localStorage
    const storedUsers = localStorage.getItem("users");
    const users = storedUsers ? JSON.parse(storedUsers) : [];
    const userIndex = users.findIndex((user) => user.user_id === 1);
    if (userIndex !== -1) {
      users[userIndex].communities = [...users[userIndex].communities, newCommunity.id];
      localStorage.setItem("users", JSON.stringify(users));
    }

    setCommunities([...uniqueCommunities, newCommunity]);
    setShowModal(false);
    setFormData({
      name: "",
    });

    // Navigate to the newly created community's page
    navigate(`/${encodeURIComponent(newCommunity.name)}`);
  };

  return (
    <div className="communities-page">
      <div className="communities-top">
        <h1 className="communities-title">All Communities</h1>
        <button className="create-community-button" onClick={() => setShowModal(true)}>Create New Community</button>
      </div>
      <Modal show={showModal} handleClose={() => setShowModal(false)}>
        <form className="create-form" onSubmit={handleFormSubmit}>
          <input
            type="text"
            name="name"
            value={formData.name}
            onChange={handleInputChange}
            placeholder="Community Name"
            required
          />
          <button type="submit">Submit</button>
        </form>
      </Modal>
      <Communities communities={uniqueCommunities} />
    </div>
  );
}

export default CommunitiesPage;
