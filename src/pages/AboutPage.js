import "../styles/AboutPage.css";

function AboutPage() {
  return (
    <div className="about-page">
      <h1 className="about-page-title">City UpLift</h1>
      <p>Our application brings value by fostering community engagement and collaboration within specific regions, connecting users' diverse interests and initiatives. It serves as a platform where individuals can come together to share ideas, resources, and solutions tailored to their locality. Through an open environment, it facilitates information exchange and the organization of activities.</p>

      <p>Furthermore, gamification incentivizes positive contributions, allowing communities to customize their experiences to meet specific goals. Users can join or create communities in their area, providing access to valuable information such as local shops, venues, events, and tips for specific causes. </p>

      <p>Moreover, city leadership can utilize this platform to monitor community dynamics and trends, potentially supporting these communities and promoting overall city welfare. Ultimately, our project empowers individuals and groups to connect, innovate, and achieve common objectives.</p>
    </div>
  );
}

export default AboutPage;
