import React, { useState, useEffect } from "react";
import "../styles/Comments.css";

const Comments = ({ comments = [], onAddComment }) => {
  const [newComment, setNewComment] = useState("");
  const [commentList, setCommentList] = useState([]);

  useEffect(() => {
    // Sort comments to show the newest first
    setCommentList(comments.slice().reverse());
  }, [comments]);

  const handleAddComment = () => {
    if (newComment.trim()) {
      const newCommentObj = { user: "john_doe", comment: newComment };
      const updatedComments = [newCommentObj, ...commentList];
      setCommentList(updatedComments);
      setNewComment("");
      if (onAddComment) {
        onAddComment(newCommentObj);
      }
    }
  };

  return (
    <div className="comments-section">
      <h2>Comments</h2>
      <div className="comment-form">
        <input
          type="text"
          value={newComment}
          onChange={(e) => setNewComment(e.target.value)}
          placeholder="Add a comment"
        />
        <button onClick={handleAddComment}>Post</button>
      </div>
      <div className="comments-list">
        {commentList.map((comment, index) => (
          <div key={index} className="comment">
            <strong>{comment.user}</strong>: {comment.comment}
          </div>
        ))}
      </div>
    </div>
  );
};

export default Comments;
