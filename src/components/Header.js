import React from "react";
import { Link, useNavigate } from "react-router-dom";
import "../styles/Header.css";

function Header() {
  const navigate = useNavigate();

  const handleExploreClick = () => {
    navigate('/explore', { replace: true });
    window.location.reload(); // This forces the page to reload
  };

  return (
    <header className="header">
      <Link to="/" className="logo-link">
        <div className="logo"></div>
      </Link>
      <nav className="nav">
        <Link to="/" className="nav-link">Home</Link>
        <Link to="/communities" className="nav-link">Communities</Link>
        <Link to="/explore" className="nav-link" onClick={handleExploreClick}>Explore</Link>
        <Link to="/about" className="nav-link">About</Link>
        <Link to="/profile" className="nav-link">Profile</Link>
      </nav>
    </header>
  );
}

export default Header;
