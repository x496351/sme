// src/components/Members.js
import React, { useState, useEffect } from "react";
import "../styles/Members.css";

const Members = ({ communityName }) => {
  const [members, setMembers] = useState([]);

  useEffect(() => {
    const storedUsers = localStorage.getItem("users");
    const users = JSON.parse(storedUsers);
    const storedCommunities = localStorage.getItem("communities");
    const communities = JSON.parse(storedCommunities);

    const community = communities.find(
      (community) => community.name === communityName
    );

    if (community) {
      const communityMembers = users.filter((user) =>
        community.user_ids.includes(user.user_id)
      );
      setMembers(communityMembers);
    }
  }, [communityName]);

  return (
    <div className="members-list">
      <h2>Members of {communityName}</h2>
      {members.length === 0 ? (
        <p>No members found for this community.</p>
      ) : (
        <ul>
          {members.map((member) => (
            <li key={member.user_id} className="member-item">
              <h3>{member.username}</h3>
              <p>Points: {member.points}</p>
              <p>Level: {member.level}</p>
              <p>Badges: {member.badges.join(", ")}</p>
              <p>Referrals: {member.referrals}</p>
              <p>Event Participation: {member.event_participation.join(", ")}</p>
              <p>Challenges:</p>
              <ul>
                {Object.entries(member.challenges).map(([challenge, status]) => (
                  <li key={challenge}>{challenge}: {status}</li>
                ))}
              </ul>
              <p>Rewards Redeemed: {member.reward_redeemed.join(", ")}</p>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default Members;
