import { Link } from "react-router-dom";
import "../styles/CommunitiesFeed.css";

const Communities = ({ communities }) => {
  return (
    <div className="communities-feed">
      {communities.map((item) => (
        <CommunityItem key={item.id} item={item} />
      ))}
    </div>
  );
};

const CommunityItem = ({ item }) => {
  return (
    <Link to={`/${item.name}`} className="community-item">
      <div className="community-item-header">
        <span className="community-item-community">{item.name}</span>
      </div>
    </Link>
  );
};

export default Communities;
