import React, { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import Comments from "./Comments";
import "../styles/Feed.css";

const Feed = ({ feed }) => {
  const sortedData = feed.sort((a, b) => b.id - a.id);
  return (
    <div className="feed">
      {sortedData.map((item, index) => (
        <FeedItem key={index} item={item} />
      ))}
    </div>
  );
};

const FeedItem = ({ item }) => {
  const navigate = useNavigate();
  const [rating, setRating] = useState(item.rating || 0);
  const [likes, setLikes] = useState(item.likes || 0);
  const [liked, setLiked] = useState(false);
  const [disliked, setDisliked] = useState(false);
  const [showComments, setShowComments] = useState(false);
  const [comments, setComments] = useState(item.comments || []);
  const [user, setUser] = useState(null);

  useEffect(() => {
    const fetchUserData = () => {
      const storedUsers = localStorage.getItem("users");
      if (storedUsers) {
        const users = JSON.parse(storedUsers);
        const postUser = users.find(u => u.username === item.posted_by);
        setUser(postUser);
      }
    };

    fetchUserData();

    const storedLiked = localStorage.getItem(`liked-${item.id}`);
    const storedDisliked = localStorage.getItem(`disliked-${item.id}`);
    const storedRating = localStorage.getItem(`rated-${item.id}`);
    if (storedLiked) {
      setLiked(true);
    }
    if (storedDisliked) {
      setDisliked(true);
    }
    if (storedRating) {
      setRating(parseFloat(storedRating));
    }
  }, [item.id, item.posted_by]);

  const handleItemClick = (event) => {
    if (
      event.target.tagName !== "BUTTON" &&
      !event.target.classList.contains("feed-item-community") &&
      !event.target.classList.contains("comments-section") &&
      !event.target.closest(".comment-form") &&
      !event.target.classList.contains("tag-link")
    ) {
      navigate(`/${encodeURIComponent(item.community)}/${item.id}`);
    }
  };

  const handleLike = (e) => {
    e.stopPropagation();
    if (disliked) {
      setLikes(likes + 2);
      setDisliked(false);
      localStorage.removeItem(`disliked-${item.id}`);
    } else {
      setLikes(likes + 1);
    }
    setLiked(true);
    localStorage.setItem(`liked-${item.id}`, true);
    updateLikes(item.id, likes + 1);
  };

  const handleDislike = (e) => {
    e.stopPropagation();
    if (liked) {
      setLikes(likes - 2);
      setLiked(false);
      localStorage.removeItem(`liked-${item.id}`);
    } else {
      setLikes(likes - 1);
    }
    setDisliked(true);
    localStorage.setItem(`disliked-${item.id}`, true);
    updateLikes(item.id, likes - 1);
  };

  const handleRatingChange = (newRating) => {
    setRating(newRating);
    localStorage.setItem(`rated-${item.id}`, newRating);
    updateRating(item.id, newRating);
  };

  const updateLikes = (id, newLikes) => {
    const storedData = JSON.parse(localStorage.getItem("data"));
    const updatedData = storedData.map((post) =>
      post.id === id ? { ...post, likes: newLikes } : post
    );
    localStorage.setItem("data", JSON.stringify(updatedData));
  };

  const updateRating = (id, newRating) => {
    const storedData = JSON.parse(localStorage.getItem("data"));
    const updatedData = storedData.map((post) =>
      post.id === id ? { ...post, rating: newRating } : post
    );
    localStorage.setItem("data", JSON.stringify(updatedData));
  };

  const toggleComments = (e) => {
    e.stopPropagation();
    setShowComments(!showComments);
  };

  const handleAddComment = (newComment) => {
    const updatedComments = [...comments, newComment];
    setComments(updatedComments);
    updateComments(item.id, updatedComments);
  };

  const updateComments = (id, newComments) => {
    const storedData = JSON.parse(localStorage.getItem("data"));
    const updatedData = storedData.map((post) =>
      post.id === id ? { ...post, comments: newComments } : post
    );
    localStorage.setItem("data", JSON.stringify(updatedData));
  };

  const handleTagClick = (tag, e) => {
    e.stopPropagation();
    navigate(`/explore?tags=${encodeURIComponent(tag)}`);
  };

  return (
    <div className="feed-item" onClick={handleItemClick}>
      <div className="feed-item-header">
        <span className="feed-item-content">{item.content}</span>
        <Link
          to={`/${item.community}`}
          className="feed-item-community"
          onClick={(e) => e.stopPropagation()}
        >
          {item.community}
        </Link>
      </div>
      <div className="feed-item-details">
        <p>{item.details}</p>
      </div>
      {user && (
        <div className="feed-item-user">
          <span>Posted by: {user.username}</span> | <span>Level: {user.level}</span> | <span>Points: {user.points}</span>
        </div>
      )}
      <div className="post-actions">
        {item.type === "post" ? (
          <div className="button-group">
            <button onClick={handleLike} disabled={liked}>
              Like
            </button>
            <button onClick={handleDislike} disabled={disliked}>
              Dislike
            </button>
            <span className="likes-count">{likes} Likes</span>
          </div>
        ) : (
          <div className="star-rating">
            {[1, 2, 3, 4, 5].map((star) => (
              <span
                key={star}
                className={`star ${star <= rating ? "filled" : ""}`}
                onClick={(e) => {
                  e.stopPropagation();
                  handleRatingChange(star);
                }}
              >
                ★
              </span>
            ))}
            <span className="current-rating">{rating.toFixed(1)} Rating</span>
          </div>
        )}
        {item.type === "event" ? (
        <div className="location">
          Location: <span className="feed-item-location">{item.location}</span>
        </div>
        ):null}
      </div>
      <div className="post-bottom">
        <div className="post-tags">
          {item.tags.map((tag, index) => (
            <span key={index} className="tag-link" onClick={(e) => handleTagClick(tag, e)}>
              {tag}
            </span>
          ))}
        </div>
        <div className="button-group">
          <button onClick={toggleComments}>Comment</button>
          <button onClick={(e) => e.stopPropagation()}>Share</button>
        </div>
      </div>  
      {showComments && <Comments comments={comments} onAddComment={handleAddComment} />}
    </div>
  );
};

export default Feed;
