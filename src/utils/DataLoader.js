export async function loadData() {
    const storage = localStorage.getItem("data");
    const communities = localStorage.getItem("communities");
    const users = localStorage.getItem("users");
    if (!storage) {
        const response = await fetch("/api/posts.json");
        const data = await response.json();
        localStorage.setItem("data", JSON.stringify(data));
    }

    if (!communities) {
        const response = await fetch("/api/communities.json");
        const data = await response.json();
        localStorage.setItem("communities", JSON.stringify(data));
    }

    if (!users) {
        const response = await fetch("/api/user_profiles.json");
        const data = await response.json();
        localStorage.setItem("users", JSON.stringify(data));
    }
}